﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCollections
{
    public class MyList<T> : ICollection<T>, IEnumerator<T>
    {
        #region fields
        private T[] _arr;
        private int _count;
        private int _currentPos;
        #endregion

        #region properties and indexer
        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException();
                return _arr[index];
            }
            set
            {
                if (index < 0 || index >= _count) throw new ArgumentOutOfRangeException();
                _arr[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public T Current
        {
            get
            {
                return _arr[_currentPos];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
        #endregion

        #region ctors
        public MyList(int capacity = 10)
        {
            _arr = new T[capacity];
            _count = 0;
            _currentPos = -1;
        }
        #endregion

        public void Add(T val)
        {
            if (_count == _arr.Length)
            {
                T[] tmp = new T[_arr.Length * 2];
                for (int i = 0; i < _arr.Length; ++i)
                {
                    tmp[i] = _arr[i];
                }
                _arr = tmp;
            }
            _arr[_count++] = val;
        }

        public void Clear()
        {
            _count = 0;
        }

        public void Insert(int index, T val)
        {
            if (index >= 0)
            {
                if (_arr.Length == _count)
                {
                    Add(val);
                }
                else if (_arr.Length > _count)
                {
                    for (int i = _count; i > index; --i)
                    {
                        _arr[i] = _arr[i - 1];
                    }
                    _arr[index] = val;
                    ++_count;
                }

                else
                {
                    T[] tmp = new T[_arr.Length * ((index / _arr.Length) + 1)];
                    for (int i = 0; i < _arr.Length; ++i)
                    {
                        tmp[i] = _arr[i];
                    }
                    _arr = tmp;
                    _arr[index] = val;
                    _count = index;
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public bool Contains(T item)
        {
            bool isContains = false;
            for (int i = 0; i < _count; i++)
            {
                if (item.Equals(_arr[i]))
                {
                    isContains = true;
                    break;
                }
            }
            return isContains;

        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (arrayIndex >= _count || arrayIndex < 0)
                throw new ArgumentOutOfRangeException();
            else
            {
                for (int i = 0; i < _count; i++)
                {
                    array[i] = _arr[i];
                }
            }
        }

        public bool Remove(T item)
        {
            bool isRemoved = false;
            for (int i=0; i<_count; i++)
            {
                if (item.Equals(_arr[i]))
                {
                    RemoveAt(i);
                    isRemoved = true;
                    break;
                }
            }
            return isRemoved;
        }

        public void RemoveAt(int index)
        {
            if (index < _count - 1 && index>=0)
            {
                while (index < _count - 1)
                {
                    _arr[index] = _arr[index + 1];
                    index++;
                }
                --_count;
            }
            else
                throw new ArgumentOutOfRangeException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        public void Dispose()
        {
            Reset();
        }

        public bool MoveNext()
        {
            ++_currentPos;
            return _currentPos < _count;
        }

        public void Reset()
        {
            _currentPos = -1;
        }
    }
}
