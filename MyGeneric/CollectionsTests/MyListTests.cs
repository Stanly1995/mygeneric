﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyCollections;

namespace CollectionsTests
{
    [TestClass]
    public class MyListTests
    {
        [TestMethod]
        public void CtorTestMethod()
        {
            MyList<int> list = new MyList<int>();
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            Assert.AreEqual(false, list.IsReadOnly);
        }

        [TestMethod]
        public void AddTestMethod()
        {
            MyList<int> list = new MyList<int>(4);
            Assert.AreNotEqual(null, list);
            Assert.AreEqual(0, list.Count);
            list.Add(10);
            Assert.AreEqual(1, list.Count);

            for (int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(101, list.Count);
        }

        [TestMethod]
        public void ClearTestMethod()
        {
            MyList<int> list = new MyList<int>(4);
            for (int i = 0; i < 100; ++i)
            {
                list.Add(i);
            }
            Assert.AreEqual(100, list.Count);
            list.Clear();
            Assert.AreEqual(0, list.Count);
        }
        [TestMethod]
        public void IndexTestMethod()
        {
            int[] arr = { 1, 5, 10, 15, 25, 3, 8, 8 };
            MyList<int> list = new MyList<int>();
            foreach (int val in arr)
            {
                list.Add(val);
            }
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTestMethod_0()
        {
            MyList<int> list = new MyList<int>();
            list[-1] = 100;
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTestMethod_1()
        {
            MyList<int> list = new MyList<int>();
            list[1] = 100;
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTestMethod_2()
        {
            MyList<int> list = new MyList<int>();
            int x = list[-1];
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeTestMethod_3()
        {
            MyList<int> list = new MyList<int>();
            int x = list[1];
        }

        [TestMethod]
        public void InsertTestMethod_0()
        {
            int[] arr = { 1, 2, 3, 4 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Insert(3, 4);
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        public void InsertTestMethod_1()
        {
            int[] arr = { 1, 4, 2, 3 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Insert(1, 4);
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        public void InsertTestMethod_2()
        {
            int[] arr = { 1, 2, 3, 0, 0, 0, 0, 4 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Insert(8, 4);
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void InsertTestMethod_3()
        {
            MyList<int> list = new MyList<int>();
            list.Insert(-1, 4);
        }

        [TestMethod]
        public void RemoveAtTestMethod_0()
        {
            int[] arr = { 1, 2, 4 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.RemoveAt(2);
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveAtTestMethod_1()
        {
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.RemoveAt(-1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveAtTestMethod_2()
        {
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.RemoveAt(3);
        }

        [TestMethod]
        public void RemoveTestMethod_0()
        {
            int[] arr = { 1, 2, 4, 3 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(3);
            Assert.AreEqual(true, list.Remove(3));
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        public void RemoveTestMethod_1()
        {
            int[] arr = { 1, 2, 3, 4, 3 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(3);
            Assert.AreEqual(false, list.Remove(5));
            for (int i = 0; i < list.Count; ++i)
            {
                Assert.AreEqual(arr[i], list[i]);
            }
        }

        [TestMethod]
        public void ContainsTestMethod_0()
        {
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            Assert.AreEqual(true, list.Contains(3));
        }

        [TestMethod]
        public void ContainsTestMethod_1()
        {
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            Assert.AreEqual(false, list.Contains(5));
        }

        [TestMethod]

        public void CopyToTestMethod_0()
        {
            int[] arr = { 1, 2, 3, 4 };
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            int[] newArr = new int[list.Count];
            for (int i=0; i < newArr.Length; i++)
            {
                list.CopyTo(newArr, i);
            }
            for (int i = 0; i < newArr.Length; ++i)
            {
                Assert.AreEqual(arr[i], newArr[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CopyToTestMethod_1()
        {
            MyList<int> list = new MyList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            int[] newArr = new int[list.Count];
                list.CopyTo(newArr, 5);
        }
    }
}
